const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

//express
const port = process.env.PORT|| 4000;
const app = express();


var bodyParser = require('body-parser');


// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.o8syqdi.mongodb.net/001ecommerceapi?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));



//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
/*app.use(cors({
        origin: 'https://ecommerce-be-tau.vercel.app/'
    }));*/
/*app.use(cors({
        origin: 'https://reactjs-ecommercebe-app.vercel.app'
    }));*/
app.use(cors({
        origin: '*'
    }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('json spaces', 1);

// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }))


// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }))

//links
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`Server running at localhost:${port}`));
