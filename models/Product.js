const mongoose = require("mongoose");

let productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Product Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stock should be specified."]
	},
	image: {
		type: String,
		default: [true, "Product Image is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("products", productSchema);
