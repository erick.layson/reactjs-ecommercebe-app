const mongoose = require("mongoose");
/*const moment = require('moment-timezone');
const {format} = require('date-fns');



let ts = Date.now();
let current = new Date();
const timestamp = new Date(Date.UTC(current.getFullYear(), 
current.getMonth(),current.getDate(),current.getHours(), 
current.getMinutes(),current.getSeconds(), current.getMilliseconds()));

let currentb = current.toLocaleString('fil-PH', {
    timeZone: 'Asia/Manila',
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZoneName: 'short',
  });
*/


let orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Total Order Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default:() => Math.floor(Date.now() + (8 * 60 * 60 * 1000)) 
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	isntTransacted: {
		type: Boolean,
		default: true
	},
	products:[{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			productImage: {
				type: String,
				default: [true, "Product Image is required"]
			}
		}]
});

module.exports = mongoose.model("orders", orderSchema);
