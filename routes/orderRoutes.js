const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
var bodyParser = require('body-parser')

router.post("/checkout/:productId", auth.verify, orderControllers.order);
router.get("/myOrders", auth.verify, orderControllers.retrieveOrders);
router.get("/allOrders", auth.verify, orderControllers.retrieveAllOrders);

router.get("/:orderId", orderControllers.viewProduct);



//transactOrder
router.patch("/transact/:orderId", auth.verify, orderControllers.transactOrder);



module.exports = router;
